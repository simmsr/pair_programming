from random import randint

def prisonerProblem():
    listOfPrisoners = ['0']*99 + ['L']*1
    #print(listOfPrisoners)
    length = len(listOfPrisoners)
    counter = 0
    tries = 0
    while counter < 100:
        i = randint(0, length - 1)
        #print(i)
        if listOfPrisoners[i] == '0':
            listOfPrisoners[i] = '1'
        elif listOfPrisoners[i] == '1':
            counter += 1
        tries += 1
    print("100 prisoners have entered the room. This took", tries, "tries")

prisonerProblem()
