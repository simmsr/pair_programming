

def possiblePairs(n):
    # The number of possible independent pairs in a group of n people
    return n*(n-1)/2

def birthdayParadox(p):
    noOfPairs = possiblePairs(p)
    # probability of two people having the same birthday
    return( 1 -  (364/365) ** noOfPairs)


p1 = 23
print(birthdayParadox(p1))

p2 = 100
print(birthdayParadox(p2))

p3 = 164
print(birthdayParadox(p3))
