# The Birthday Paradox

## Pair Programming - Rebecca and Laura

With 23 people we have 253 pairs:

```python
23 * 22 / 2 = 253
```

The chance of 2 people having different birthdays is:

```python
1 - 1/365 = 364/365 = .997260
```

making 253 comparisons and having them all be different is

```python
(364/365) ** 253 = 0.4995
```

The chance we find a match is

```python
1 – 49.95% = 50.05%
```

 
